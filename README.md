# Zuora Error Code Explainer

## **This project is no longer maintained - please see [tools-mcatee-io/index](https://gitlab.com/tools-mcatee-io/index) for the most recent version**.

A tool for resolving Zuora transaction errors to their:

- **Error category codes** - which identifies the type of error that has occurred
- **Resource codes** - indicates the Billing REST API resource to which the error is related

## See Also

- [Zuora - Error Handling Overview](https://www.zuora.com/developer/api-references/api/overview/#section/Error-Handling)
- [Zuora - Error Category Codes](https://www.zuora.com/developer/api-references/api/overview/#section/Error-Handling/Error-Category-Codes)
- [Zuora - Resource Codes](https://knowledgecenter.zuora.com/Zuora_Central_Platform/API/AA_REST_API/Resource_codes_for_Billing_and_Payments_REST_API")